'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Foci = new Module('foci');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Foci.register(function(app, auth, database, system) {

  //We enable routing. By default the Package Object is passed to the routes
  Foci.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  Foci.menus.add({
    title: 'About',
    link: 'about',
    roles: ['authenticated', 'anonymous'],
    menu: 'focimain'
  });

  Foci.menus.add({
    title: 'Library',
    link: 'library',
    roles: ['authenticated'],
    menu: 'focimain'
  });

  Foci.menus.add({
    title: 'Account',
    link: 'account',
    roles: ['authenticated'],
    menu: 'focimain'
  });

  Foci.menus.add({
    title: 'CONTENT',
    link: 'content',
    roles: ['authenticated'],
    menu: 'admin'
  })

  // Set views path, template engine and default layout
  app.set('views', __dirname + '/server/views');

  //Foci.aggregateAsset('css', 'foci.css');

  Foci.angularDependencies(['mean.system']);

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Foci.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Foci.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Foci.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return Foci;
});
