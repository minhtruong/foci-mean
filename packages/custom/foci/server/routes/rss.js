'use strict';

/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(RSS, app) {

    var rss = require('../controllers/rss')(RSS);

    app.route('/api/rss/create')
        .post(rss.parse);
};
