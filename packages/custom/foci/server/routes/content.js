'use strict';

/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(Content, app, auth, database) {

    var content = require('../controllers/content')(Content);

    app.route('/api/content')
        .post(content.create);

    app.route('/api/content/update')
        .post(content.update);

    app.route('/api/content/all')
        .get(content.all);

    app.route('/api/content/all/active')
        .get(content.allActive);

    app.route('/api/content/parse')
        .post(content.parseText);

    app.route('/api/content/demo')
        .get(content.getDemo);

    app.route('/api/rss/refresh')
        .get(content.refreshRss);

    app.route('/api/rss/auto-refresh/start')
        .get(content.startAutoRefreshRss);

    app.route('/api/rss/auto-refresh/end')
        .get(content.endAutoRefreshRss);

    app.param('contentId', content.find);

    app.route('/api/content/:contentId')
        .get(function(req, res) {
            res.send(req.content);
        });
};
