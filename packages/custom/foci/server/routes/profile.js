'use strict';

/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(Profile, app) {

    var content = require('../controllers/profile')(Profile);

    app.route('/api/profile/get')
        .post(content.get);

    app.route('/api/profile/update')
        .post(content.update);
};
