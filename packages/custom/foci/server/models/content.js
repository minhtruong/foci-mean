'use strict';

var mongoose = require('mongoose'),
    Schema    = mongoose.Schema;

var ContentSchema = new Schema({
    title: String,
    title_normalized: String,
    author: String,
    image: Buffer,
    type: String,
    text: String,
    parsed_text: {},
    is_active: Boolean,
    is_demo: Boolean,
    frame_size: Number
}, { timestamps: { createdAt: 'created_at' } } );

var Content = mongoose.model('Content', ContentSchema);
module.exports = Content;