'use strict';

var mongoose = require('mongoose'),
    Schema    = mongoose.Schema;

var ProfileScheme = new Schema({
    name: String,
    email: String,
    grade: String,
    school: String,
    user_id: String
});

var Profile = mongoose.model('Profile', ProfileScheme);
module.exports = Profile;