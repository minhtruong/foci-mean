'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    http = require('http');
    //Content = mongoose.model('Content');

module.exports = function() {
    return {

        /**
         * Parse rss feed
         */
        parse: function(feed) {
            var options = {
                host: '104.239.134.254',
                port: 8000,
                path: '/parseRss',
                method: 'POST'
            };

            var parseReq = http.request(options, function(parseRes) {
                var parsedText = "";
                parseRes.setEncoding('utf8');
                parseRes.on('data', function (chunk) {
                    parsedText += chunk;
                });
                parseRes.on('end', function () {
                    console.log(new Date().toLocaleString() + ": Refreshed " + feed.text);
                    feed.parsed_text = JSON.parse(parsedText);
                    feed.save(function(err) {
                        if (err) {
                            return false;
                        }
                        else {
                            return true;
                        }
                    });
                });
            });

            parseReq.write(feed.frame_size + " " + feed.text);
            parseReq.end();
        }
    };
}

