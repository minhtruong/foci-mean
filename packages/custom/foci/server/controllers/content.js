'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    http = require('http'),
    Content = mongoose.model('Content'),
    rss = require('./rss');

module.exports = function(FociContent, RSS) {
    var rss = require('./rss')(RSS);
    var timer;
    var autoRefreshTime = 60 * 60 * 1000;

    return {
        /**
         * Create content
         */
        create: function(req, res, next) {
            var content = new Content(req.body);
            content.title_normalized = content.title.toLowerCase();

            req.assert('title', 'You must enter a name').notEmpty();
            req.assert('author', 'You must enter a valid email address').notEmpty();
            req.assert('type', 'You must choose a type').notEmpty();
            req.assert('text', 'You must enter text').notEmpty();

            var options;
            if(content.type != "RSS") {
                options = {
                    host: '104.239.134.254',
                    port: 8000,
                    path: '/jsonparse',
                    method: 'POST'
                };
            }
            else {
                options = {
                    host: '104.239.134.254',
                    port: 8000,
                    path: '/parseRss',
                    method: 'POST'
                };
            }

            var parsedText = "";

            var parseReq = http.request(options, function(parseRes) {
                parseRes.setEncoding('utf8');
                parseRes.on('data', function (chunk) {
                    parsedText += chunk;
                });
                parseRes.on('end', function () {
                    content.parsed_text = JSON.parse(parsedText);
                    content.save(function(err) {
                        if (err) {
                            switch (err.code) {
                                default:
                                    var modelErrors = [];

                                    if (err.errors) {

                                        for (var x in err.errors) {
                                            modelErrors.push({
                                                param: x,
                                                msg: err.errors[x].message,
                                                value: err.errors[x].value
                                            });
                                        }

                                        res.status(400).json(modelErrors);
                                    }
                            }
                            return res.status(400);
                        }

                        res.json(content);
                        res.status(200);
                    });
                });
            });

            console.log(content);
            parseReq.write(content.frame_size + " " + content.text);
            parseReq.end();

            var errors = req.validationErrors();
            if (errors) {
                return res.status(400).send(errors);
            }
        },

        /**
         * Find content by id
         */
        find: function(req, res, next, contentId) {
            Content.findOne({
                _id: contentId
            }).exec(function(err, content) {
                if (err) return next(err);
                if (!content) return next(new Error('Failed to load User ' + id));
                req.content = content;
                next();
            });
        },

        /**
         * Get all content
         */
        all: function(req, res) {
            Content.find({})
                .sort({ title_normalized: 1, title: 1 })
                .exec(function(err, content) {
                    if (err) return res.json(err);
                    if (!content) return res.json(err);
                    res.json(content);
                    res.status(200);
                });
        },

        /**
         * Get all content
         */
        allActive: function(req, res) {
            Content.find({ is_active: true })
                .sort({ title_normalized: 1, title: 1 })
                .exec(function(err, content) {
                    if (err) return res.json(err);
                    if (!content) return res.json(err);
                    res.json(content);
                    res.status(200);
                });
        },

        parseText: function(req, res) {
            var options = {
                host: '104.239.134.254',
                port: 8000,
                path: '/jsonparse',
                method: 'POST'
            };

            var parsedText = "";

            var parseReq = http.request(options, function(parseRes) {
                parseRes.setEncoding('utf8');
                parseRes.on('data', function (chunk) {
                    parsedText += chunk;
                });
                parseRes.on('end', function() {
                    res.json(JSON.parse(parsedText));
                    res.status(200);
                });
            });

            parseReq.write(req.body.text);
            parseReq.end();
        },

        /**
         * Find demo passage
         */
        getDemo: function(req, res) {
            Content.findOne({
                is_active: true,
                is_demo: true
            }).exec(function(err, content) {
                if (err) return res.json(err);
                if (!content) return res.json(err);
                res.json(content);
                res.status(200);
            });
        },

        update: function(req, res) {
            var updated = req.body;
            Content.findOne({
                _id: updated._id
            }).exec(function(err, content) {
                if (err) return next(err);
                if (!content) return next(new Error('Failed to load content'));

                content.is_active = updated.is_active;
                content.text = updated.text;
                content.type = updated.type;
                content.author = updated.author;
                content.title = updated.title;
                content.title_normalized = updated.title.toLowerCase();
                content.parsed_text = updated.parsed_text;
                content.is_demo = updated.is_demo;
                content.frame_size = updated.frame_size;
                content.save(function(err) {
                    if (err) {
                        switch (err.code) {
                            default:
                                var modelErrors = [];

                                if (err.errors) {

                                    for (var x in err.errors) {
                                        modelErrors.push({
                                            param: x,
                                            msg: err.errors[x].message,
                                            value: err.errors[x].value
                                        });
                                    }

                                    res.status(400).json(modelErrors);
                                }
                        }
                        return res.status(400);
                    }
                });

                res.json(content);
                res.status(200);
            });
        },

        refreshRss: function(req, res) {
            Content.find({
                type: 'RSS',
                is_active: true
            }).exec(function(err, feeds) {
                for(var i = 0; i < feeds.length; i++) {
                    rss.parse(feeds[i]);
                }

                res.json("Refreshing RSS.");
                res.status(200);
            });
        },

        startAutoRefreshRss: function(req, res) {
            Content.find({
                type: 'RSS',
                is_active: true
            }).exec(function(err, feeds) {
                timer = setInterval(function() {
                    for(var i = 0; i < feeds.length; i++) {
                        rss.parse(feeds[i]);
                    }
                }, autoRefreshTime);

                res.json("Auto refresh of RSS has started.");
                res.status(200);
            });
        },

        endAutoRefreshRss: function(req, res) {
            clearInterval(timer);
            res.json("Auto refresh of RSS has ended.");
            res.status(200);
        }
    };
}

