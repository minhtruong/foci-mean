'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    http = require('http'),
    Profile = mongoose.model('Profile');

module.exports = function(FociProfile) {
    return {

        /**
         * Create content
         */
        get: function(req, res, next) {
            var user = req.body;
            console.log(req.body);
            console.log(user);

            Profile.findOne({
                user_id: user._id
            }).exec(function(err, profile) {
                if (err) return next(err);
                if (!profile)  {
                    var prof = new Profile();
                    prof.name = user.name;
                    prof.email = user.email;
                    prof.user_id = user._id;

                    console.log(prof);
                    prof.save(function(err) {
                        if (err) {
                            console.log("###ERROR###");
                            switch (err.code) {
                                default:
                                    var modelErrors = [];

                                    if (err.errors) {

                                        for (var x in err.errors) {
                                            modelErrors.push({
                                                param: x,
                                                msg: err.errors[x].message,
                                                value: err.errors[x].value
                                            });
                                        }

                                        res.status(400).json(modelErrors);
                                    }
                            }
                            return res.status(400);
                        }
                    });
                    res.json(prof);
                    return res.status(200);
                }
                res.json(profile);
                res.status(200);
            });
        },

        update: function(req, res) {
            var updated = req.body;
            Profile.findOne({
                _id: updated._id
            }).exec(function(err, profile) {
                if (err) return next(err);
                if (!profile) return next(new Error('Failed to load profile'));

                profile.name = updated.name;
                profile.email = updated.email;
                profile.grade = updated.grade;
                profile.school = updated.school;
                profile.save(function(err) {
                    if (err) {
                        switch (err.code) {
                            default:
                                var modelErrors = [];

                                if (err.errors) {

                                    for (var x in err.errors) {
                                        modelErrors.push({
                                            param: x,
                                            msg: err.errors[x].message,
                                            value: err.errors[x].value
                                        });
                                    }

                                    res.status(400).json(modelErrors);
                                }
                        }
                        return res.status(400);
                    }
                });

                res.json(profile);
                res.status(200);
            });
        }
    };
}

