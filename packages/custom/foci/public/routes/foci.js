'use strict';

angular.module('mean.foci').config(['$viewPathProvider', '$stateProvider',
  function($viewPathProvider, $stateProvider) {
      $viewPathProvider.override('system/views/index.html', 'foci/views/index.html');
      $viewPathProvider.override('users/views/register.html', 'foci/views/register.html');
      $viewPathProvider.override('users/views/login.html', 'foci/views/login.html');
      $viewPathProvider.override('users/views/index.html', 'foci/views/auth.html');
      $stateProvider
          .state('about', {
              url: '/about',
              templateUrl: 'foci/views/about.html'
          })
          .state('library', {
              url: '/library',
              templateUrl: 'foci/views/library.html'
          })
          .state('read', {
              url: '/library/read/:contentId',
              templateUrl: 'foci/views/read.html'
          })
          .state('rss', {
              url: '/library/rss/read/:contentId',
              templateUrl: 'foci/views/rss.html'
          })
          .state('account', {
              url: '/account',
              templateUrl: 'foci/views/account.html'
          })
          .state('account-edit', {
              url: '/account/edit',
              templateUrl: 'foci/views/account-edit.html'
          })
          .state('content', {
              url: '/admin/content',
              templateUrl: 'foci/views/content-list.html'
          })
          .state('content-add', {
              url: '/admin/content/add',
              templateUrl: 'foci/views/content-add.html'
          })
          .state('content-edit', {
              url: '/admin/content/:contentId/edit',
              templateUrl: 'foci/views/content-add.html'
          })
          .state('parser-test', {
              url: '/parser-test',
              templateUrl: 'foci/views/parser-test.html'
          });
  }
]);