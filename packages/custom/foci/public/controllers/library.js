'use strict';

angular.module('mean.foci').controller('LibraryController', ['$scope', '$http', 'MeanUser',
    function($scope, $http, MeanUser) {

        $scope.library = [];

        var auth = {
            authenticated: MeanUser.loggedin,
            user: MeanUser.user,
            isAdmin: MeanUser.isAdmin
        };

        if(auth.isAdmin) {
            $http.get('/api/content/all/active').success(function (data) {
                $scope.library = data;
            });
        }
        else {
            $http.get('/api/content/all/active').success(function (data) {
                $scope.library = data;
            });
        }

        $scope.read = function(type, contentId) {
            if(type != 'RSS')
                window.location = "/library/read/" + contentId;
            else
                window.location = "/library/rss/read/" + contentId;
        };
    }
]);
