'use strict';

angular.module('mean.foci').controller('ContentAddController', ['$scope', '$http', '$stateParams', 'MeanUser',
    function($scope, $http, $stateParams, MeanUser) {
        var auth = {
            authenticated: MeanUser.loggedin,
            user: MeanUser.user,
            isAdmin: MeanUser.isAdmin
        };
        $scope.isEditing = false;
        $scope.content = {
            title: "",
            author: "",
            type: "",
            text: "",
            is_active: true,
            is_demo: false,
            frame_size: 3
        };

        if($stateParams.hasOwnProperty('contentId')) {
            $scope.isEditing = true;
            var contentId = $stateParams.contentId;
            $http.get('/api/content/' + contentId).success(function (data) {
                console.log(data);
                $scope.content = data;
            });
        }
        else {
            $scope.isEditing = false;
        }

        $scope.save = function() {
            if(!$scope.isEditing) {
                $http.post('/api/content', $scope.content).success(function (res) {
                    console.log(res);
                    window.location = "/admin/content";
                });
            }
            else {
                $http.post('/api/content/update', $scope.content).success(function (res) {
                    console.log(res);
                    window.location = "/admin/content";
                });
            }
        };
    }
]);
