'use strict';

/* jshint -W098 */
angular.module('mean.foci').controller('FociController', ['$scope', 'Global', 'Foci',
  function($scope, Global, Foci) {
    $scope.global = Global;
    $scope.package = {
      name: 'foci'
    };
  }
]);
