'use strict';

angular.module('mean.foci').controller('HomeController', ['$scope', '$http', 'MeanUser',
    function($scope, $http, MeanUser) {

        var auth = {
            authenticated: MeanUser.loggedin,
            user: MeanUser.user,
            isAdmin: MeanUser.isAdmin
        };

        $scope.$on('fociPlayerReady', function(event, data) {
            $scope.$emit('fociPlayerStart');
        });

        $http.get('/api/content/demo').success(function (data) {
            $scope.$emit('loadPassage', data.parsed_text.frames);
        });
    }
]);
