'use strict';

angular.module('mean.foci').controller('RssController', ['$scope', '$http', '$stateParams', 'MeanUser',
    function($scope, $http, $stateParams, MeanUser) {

        var auth = {
            authenticated: MeanUser.loggedin,
            user: MeanUser.user,
            isAdmin: MeanUser.isAdmin
        };

        $scope.content = {};
        $scope.feed =[];
        $scope.mode = 'list';

        $scope.readPost = function(index) {
            $scope.mode = 'reading';
            console.log($scope.feed[index].content.frames.frames);
            $scope.$emit('loadPassage', $scope.feed[index].content.frames.frames);
        };

        $scope.$on('fociPlayerReady', function(event, data) {
            $scope.$emit('fociPlayerStart');
        });

        angular.element(document).ready(function () {
            if($stateParams.hasOwnProperty('contentId')) {
                var contentId = $stateParams.contentId;
                $http.get('/api/content/' + contentId).success(function (data) {
                    console.log(data);
                    $scope.content = data;
                    $scope.feed = data.parsed_text.posts;
                });
            }
        });
    }
]);
