'use strict';

angular.module('mean.foci').controller('ParserTestController', ['$scope', '$http', '$stateParams', 'MeanUser',
    function($scope, $http, $stateParams, MeanUser) {
        var auth = {
            authenticated: MeanUser.loggedin,
            user: MeanUser.user,
            isAdmin: MeanUser.isAdmin
        };

        $scope.text = "";
        $scope.frameSize = "3";
        $scope.parsedText = "";
        $scope.showResult = false;

        $scope.parse = function() {
            console.log($scope.text);
            var data = {
                text: "-" +$scope.frameSize + " " + $scope.text
            };
            $http.post('/api/content/parse', data).success(function (res) {
                $scope.parsedText = JSON.stringify(res, null, 2);
                $scope.$emit('loadPassage', res.frames);
                $scope.showResult = true;
            });
        };
    }
]);
