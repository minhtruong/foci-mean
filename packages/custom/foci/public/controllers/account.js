'use strict';

angular.module('mean.foci').controller('AccountController', ['$scope', '$http', 'MeanUser',
    function($scope, $http, MeanUser, Alert) {

        $scope.profile;

        var auth = {
            authenticated: MeanUser.loggedin,
            user: MeanUser.user,
            isAdmin: MeanUser.isAdmin
        };
        $scope.user = auth.user;

        $http.post('/api/profile/get', $scope.user).success(function (data) {
            $scope.profile = data;
        });

        $scope.save = function() {
            $http.post('/api/profile/update', $scope.profile).success(function (data) {
                $scope.profile = data;
                window.location.pathname = '/account';
            });
        };

        $scope.edit = function() {
            window.location.pathname = '/account/edit';
        };

        $scope.cancel = function() {
            window.location.pathname = '/account';
        };
    }
]);
