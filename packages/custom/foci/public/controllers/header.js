'use strict';

angular.module('mean.foci').controller('HeaderController', ['$scope', '$rootScope', 'Menus', 'MeanUser', '$state',
  function($scope, $rootScope, Menus, MeanUser, $state) {
    
    var vm = this;

    vm.menus = {};
    vm.hdrvars = {
      authenticated: MeanUser.loggedin,
      user: MeanUser.user, 
      isAdmin: MeanUser.isAdmin
    };

    // Default hard coded menu items for main menu
    var defaultMainMenu = [];

    // Query menus added by modules. Only returns menus that user is allowed to see.
    function queryMenu(name, defaultMenu) {
      Menus.query({
        name: name,
        defaultMenu: defaultMenu
      }, function(menu) {
        vm.menus[name] = menu;
      });
    }

    // Query server for menus and check permissions
    queryMenu('focimain', defaultMainMenu);
    queryMenu('admin', []);
    queryMenu('account', []);


    $scope.isCollapsed = true;

    $rootScope.$on('loggedin', function() {
      queryMenu('focimain', defaultMainMenu);
      queryMenu('admin', []);
      queryMenu('account', []);

      vm.hdrvars = {
        authenticated: MeanUser.loggedin,
        user: MeanUser.user,
        isAdmin: MeanUser.isAdmin
      };

      //if(window.location.pathname != "/library") {
      //  window.location.pathname = "/library";
      //}
    });

    vm.logout = function(){
      $scope.isCollapsed = true;
      MeanUser.logout();
    };

    $rootScope.$on('logout', function() {
      vm.hdrvars = {
        authenticated: false,
        user: {},
        isAdmin: false
      };
      queryMenu('focimain', defaultMainMenu);
      queryMenu('admin', []);
      queryMenu('account', []);
      $state.go('home');
    });

    $scope.linkClicked = function() {
      $scope.isCollapsed = true;
    };
  }
]);
