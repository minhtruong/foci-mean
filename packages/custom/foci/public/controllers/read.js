'use strict';

angular.module('mean.foci').controller('ReadController', ['$scope', '$http', '$stateParams', 'MeanUser',
    function($scope, $http, $stateParams, MeanUser) {

        var auth = {
            authenticated: MeanUser.loggedin,
            user: MeanUser.user,
            isAdmin: MeanUser.isAdmin
        };

        $scope.content = {};

        angular.element(document).ready(function () {
            if($stateParams.hasOwnProperty('contentId')) {
                var contentId = $stateParams.contentId;
                $http.get('/api/content/' + contentId).success(function (data) {
                    console.log(data);
                    $scope.content = data;
                    $scope.$emit('loadPassage', data.parsed_text.frames);
                });
            }
        });
    }
]);
