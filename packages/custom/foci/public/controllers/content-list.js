'use strict';

angular.module('mean.foci').controller('ContentListController', ['$scope', '$http', '$stateParams', 'MeanUser',
    function($scope, $http, $stateParams, MeanUser) {
        var auth = {
            authenticated: MeanUser.loggedin,
            user: MeanUser.user,
            isAdmin: MeanUser.isAdmin
        };

        $scope.content = [];

        $http.get('/api/content/all').success(function (data) {
            console.log(data);
            $scope.content = data;
        });
    }
]);
