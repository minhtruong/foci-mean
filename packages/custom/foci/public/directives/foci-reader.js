'use strict';

angular.module('mean.foci')

    .directive('focireader', function () {
        return {
            restrict: 'E',
            templateUrl: '/foci/views/foci-reader.html',
            controller: function ($scope, $http, $sce, $timeout) {
                $scope.leftPosition = '100px';
                $scope.wpmOptions = [];
                $scope.timer;
                $scope.frames = [];
                $scope.player = {
                    userId: 0,
                    currentText: "Loading...",
                    wpm: 250,
                    isRunning: false,
                    framesRead: 0,
                    readPerc: 0,
                    controlsEnabled: false,
                    chapterText: "",
                    currentFrame: 0
                };

                for (var i = 100; i <= 700; i += 50) {
                    var obj = {
                        label: i + " WPM",
                        value: i
                    }
                    $scope.wpmOptions.push(obj);
                };

                $scope.$on('loadPassage', function (event, data) {
                    $scope.loadPassage(data);
                });

                $scope.$on('fociPlayerStart', function (event, data) {
                    $scope.read()
                });

                $scope.loadPassage = function (data) {
                    $scope.player.currentFrame = 0;
                    $scope.player.framesRead = 0;
                    $scope.player.wpm = 250;
                    $scope.frames = data;
                    $scope.player.controlsEnabled = true;
                    $scope.displayWord($scope.player.currentFrame, $scope.frames);
                    $scope.$emit('fociPlayerReady');
                };

                $scope.read = function () {
                    var player = $scope.player;
                    player.isRunning = true;

                    var frame = $scope.frames[player.currentFrame];
                    var text = frame.text;

                    $scope.positionText(frame.text, frame.centerChar);

                    var coefficient = frame.time;
                    var secondsPerWord = 60 / player.wpm;
                    var timeout = coefficient * secondsPerWord * 1000;

                    $scope.timer = setTimeout(function () {
                        player.framesRead += 1;
                        player.readPerc = player.framesRead / $scope.frames.length * 100;
                        player.currentFrame++;

                        if (player.framesRead < $scope.frames.length) {
                            var coefficient = frame.time;
                            var secondsPerWord = 60 / player.wpm;
                            var timeout = coefficient * secondsPerWord * 1000;
                            $scope.read();
                        }
                        else {
                            player.readPerc = 100;
                            $scope.pause();
                        }
                    }, timeout)
                };

                $scope.displayWord = function (currentFrame, frames) {
                    var frame = frames[currentFrame];
                    $scope.positionText(frame.text, frame.centerChar);
                };

                $scope.pause = function () {
                    $scope.player.isRunning = false;
                    //$scope.$apply();
                };

                $scope.restart = function () {
                    var player = $scope.player;
                    if (player.controlsEnabled) {
                        player.currentFrame = 0;
                        player.framesRead = 0;
                        player.readPerc = 0;
                        $scope.displayWord(player.currentFrame, $scope.frames);
                    }
                };

                $scope.playPause = function () {
                    if ($scope.player.controlsEnabled) {
                        clearTimeout($scope.timer);
                        if ($scope.player.isRunning) {
                            $scope.pause();
                        } else {
                            $scope.read();
                        }
                    }
                };

                $scope.rewind = function () {
                    if ($scope.player.controlsEnabled) {
                        clearTimeout($scope.timer);
                        $scope.pause();
                        var player = $scope.player;
                        if (player.currentFrame > 0) {
                            player.currentFrame--;
                            player.framesRead--;
                        }
                        $scope.read();
                    }
                };

                $scope.getControlsClass = function (state) {
                    if (state)
                        return "";
                    else
                        return "disabled";
                };

                $scope.positionText = function (text, charPos) {
                    var textWidth = $scope.getWidth(text.substring(0, charPos + 1));
                    var charWidth = $scope.getWidth(text.substring(charPos, charPos + 1));
                    var boxWidth = $("#foci-reader-text").width() * .42 + (charWidth / 2);
                    var position = boxWidth - textWidth;
                    $scope.leftPosition = (position + 1) + "px";
                    var text = text.substring(0, charPos - 1) + '<span class="text-red">' + text.substring(charPos - 1, charPos) + "</span>" + text.substring(charPos, text.length);
                    $scope.player.currentText = $sce.trustAsHtml(text);
                    $timeout($scope.showActiveFrame, 10);
                };

                $scope.getWidth = function (text) {
                    var el = $("#measure-text").html(text);
                    return $scope.textWidth(el);
                };

                $scope.textWidth = function (el) {
                    var html_org = el.html();
                    var html_calc = '<span>' + html_org + '</span>';
                    el.html(html_calc);
                    var width = el.find('span')[0].offsetWidth;
                    el.html(html_org);
                    return width;
                };

                $scope.showActiveFrame = function () {
                    var player = $scope.player;
                };

                $scope.saveProgress = function () {
                    console.log("Saving progres...");
                    var player = $scope.player;
                    //player.userId = Auth.currentUser().id;
                    $http.post('/api/book/progress', player).success(function (data) {});
                };

                $scope.changeWpm = function (newWpm) {
                    console.log(newWpm);
                    $scope.player.wpm = newWpm;
                };
            },

        };
    });